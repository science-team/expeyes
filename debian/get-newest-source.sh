#!/bin/sh

version=$2
orig_tgz=$(basename $3)
newdir=$(echo $orig_tgz | sed -e 's/.orig.tar.gz//' -e 's/_/-/')

wd=$(pwd)

cd ..
tar xzf $orig_tgz
mv expeyes-programs-$version $newdir
cd $newdir
# remove symlinks pointing outside the source tree
# fix for a lintian error.
for f in $(find . -type l); do 
    if echo $(readlink $f)| grep -Eq '^/'; then 
	rm $f
    fi
done
mkdir doc
cd doc
wget https://github.com/expeyes/expeyes-doc/archive/master.zip
echo "unzipping ..."
unzip master.zip > /dev/null 2>&1
mv expeyes-doc-master/* .
rm master.zip
rmdir expeyes-doc-master

cd ../..

rm $orig_tgz v${version}.tar.gz

tar czf $orig_tgz $newdir
rm -rf $newdir

echo "Created ../$orig_tgz"
